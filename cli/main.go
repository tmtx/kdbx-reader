package main

import (
	"flag"
	"fmt"
	"gitlab.com/tmtx/kdbx-reader/lib"
	"log"
	"os"
	"path/filepath"
)

var path = flag.String("path", "", "Path to kdbx file")
var title = flag.String("title", "", "Title of entry")
var showHelp = flag.Bool(`h`, false, `display help for this tool`)

const description = `
Returns password from wordspass file, given entry title
`

func errCheck(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	flag.Parse()

	if *showHelp {
		fmt.Println(description)
		flag.PrintDefaults()
		return
	}

	if *path == "" {
		log.Fatal("Must define flag `-path`")
	}

	if *title == "" {
		log.Fatal("Must define flag `-title`")
	}

	fmt.Println("Enter password: ")
	bytePassword, err := lib.ReadPasswordFromStdin()
	errCheck(err)

	if len(bytePassword) == 0 {
		log.Fatal("Must define password for file")
	}

	absPath, err := filepath.Abs(*path)
	errCheck(err)

	f, err := os.Open(absPath)
	errCheck(err)
	defer f.Close()

	entryList, protectedKey, err := lib.EncryptedEntryList(f, string(bytePassword))
	errCheck(err)

	pw := lib.RawPasswordByTitle(&entryList, protectedKey, *title)
	fmt.Println(pw)
}
