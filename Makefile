current_dir = $(shell pwd)

.PHONY: all make_cli

make_cli:
	@echo "building kdbx-reader cli >>";
	cd lib && godep restore
	go build -o $(current_dir)/build/kdbx-reader ./cli;
