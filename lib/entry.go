package lib

import (
	"bytes"
)

type entryData struct {
	Key   string `xml:"Key"`
	Value []byte `xml:"Value"`
}

type entry struct {
	Data           []entryData `xml:"String"`
	HistoryEntries []entry     `xml:"History>Entry"`
	Title          string
	Size           int
	PlainPassword  string
}

// EntryList holds information about entries in kdbx file
type EntryList struct {
	RootEntries    []entry `xml:"Root>Group>Entry"`
	Entries        []entry `xml:"Root>Group>Group>Entry"`
	GroupedEntries []entry
	Buffer         bytes.Buffer
	ProtectedCount int
}
