package lib

import (
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"encoding/xml"
	"errors"
	"io"
	"io/ioutil"
)

// PayloadBlock holds information about payload block.
// There are at least 2 payload blocks in the file.
type PayloadBlock struct {
	BlockID   [4]byte
	Hash      [32]byte
	BlockSize [4]byte
	Data      []byte
}

// PrimaryID - common across all kdbx versions:
// [0x03,0xD9,0xA2,0x9A]
// Bytes 0-3 in kdbx file
func PrimaryID(f io.ReadSeeker) ([]byte, error) {
	f.Seek(0, 0)
	primaryID := make([]byte, 4)
	_, err := f.Read(primaryID)
	if err != nil {
		return nil, err
	}

	// resetting seeker to avoid side effects
	f.Seek(0, 0)
	return primaryID, err
}

// SecondaryID - Byte 4 can be used to identify the file version (0x67 is latest, 0x66 is the KeePass 2 pre-release format and 0x55 is KeePass 1)
// [0x67,0xFB,0x4B,0xB5]
// Bytes 4-7 in kdbx file
func SecondaryID(f io.ReadSeeker) ([]byte, error) {
	f.Seek(4, 0)
	secondaryID := make([]byte, 4)
	_, err := f.Read(secondaryID)
	if err != nil {
		return nil, err
	}

	f.Seek(0, 0)
	return secondaryID, err
}

// FileVersionMinor - Bytes 8-9 in kdbx file
func FileVersionMinor(f io.ReadSeeker) (uint16, error) {
	f.Seek(8, 0)
	fileVersionMinor := make([]byte, 2)
	_, err := f.Read(fileVersionMinor)
	if err != nil {
		return 0, err
	}

	f.Seek(0, 0)
	return binary.LittleEndian.Uint16(fileVersionMinor), err
}

// FileVersionMajor - Bytes 10-11 in kdbx file
func FileVersionMajor(f io.ReadSeeker) (uint16, error) {
	// bytes 10 - 11
	f.Seek(10, 0)
	fileVersionMajor := make([]byte, 2)
	_, err := f.Read(fileVersionMajor)
	if err != nil {
		return 0, err
	}

	f.Seek(0, 0)
	return binary.LittleEndian.Uint16(fileVersionMajor), err
}

// Header - Dynamic header. Each header entry is [BYTE bID, LE WORD wSize, BYTE[wSize] bData].
// bID=0: END entry, no more header entries after this
// bID=1: COMMENT entry, unknown
// bID=2: CIPHERID, bData="31c1f2e6bf714350be5805216afc5aff" => outer encryption AES256, currently no others supported
// bID=3: COMPRESSIONFLAGS, LE DWORD. 0=payload not compressed, 1=payload compressed with GZip
// bID=4: MASTERSEED, 32 BYTEs string. See further down for usage/purpose. Length MUST be checked.
// bID=5: TRANSFORMSEED, variable length BYTE string. See further down for usage/purpose.
// bID=6: TRANSFORMROUNDS, LE QWORD. See further down for usage/purpose.
// bID=7: ENCRYPTIONIV, variable length BYTE string. See further down for usage/purpose.
// bID=8: PROTECTEDSTREAMKEY, variable length BYTE string. See further down for usage/purpose.
// bID=9: STREAMSTARTBYTES, variable length BYTE string. See further down for usage/purpose.
// bID=10: INNERRANDOMSTREAMID, LE DWORD. Inner stream encryption type, 0=>none, 1=>Arc4Variant, 2=>Salsa20
func Header(f io.ReadSeeker, resetFile bool) (map[byte][]byte, error) {
	header := make(map[byte][]byte)
	f.Seek(12, 0)
	end := false
	for end == false {
		bID := make([]byte, 1)
		_, err := f.Read(bID)
		if err != nil {
			return nil, err
		}
		if bID[0] == 0 {
			end = true
		}

		wSize := make([]byte, 2)
		_, err = f.Read(wSize)
		if err != nil {
			return nil, err
		}

		bData := make([]byte, binary.LittleEndian.Uint16(wSize))
		_, err = f.Read(bData)
		if err != nil {
			return nil, err
		}

		header[bID[0]] = bData
	}

	if resetFile == true {
		f.Seek(0, 0)
	}

	return header, nil
}

// payloadBlocks returns array of PayloadBlock structures.
// There are at least 2 payload blocks in the file, each is laid out [LE DWORD dwBlockId, BYTE[32] sHash, LE DWORD dwBlockSize, BYTE[dwBlockSize] bData].
func payloadBlocks(header map[byte][]byte, decryptedPayload []byte) ([]*PayloadBlock, error) {
	var payloadBlocks []*PayloadBlock

	r := bytes.NewReader(decryptedPayload)

	checkBytes := make([]byte, len(header[9]))
	_, err := r.Read(checkBytes)
	if err != nil {
		return payloadBlocks, err
	}

	// checkBytes MUST equal STREAMSTARTBYTES. Normally the length is 32 bytes.
	if !bytes.Equal(header[9], checkBytes) {
		return payloadBlocks, errors.New("Invalid data")
	}

	for {
		var payloadBlock PayloadBlock
		_, err = r.Read(payloadBlock.BlockID[:])
		if err != nil {
			break
		}

		_, err = r.Read(payloadBlock.Hash[:])
		if err != nil {
			break
		}

		_, err = r.Read(payloadBlock.BlockSize[:])
		if err != nil {
			break
		}

		bs := binary.LittleEndian.Uint16(payloadBlock.BlockSize[:])

		tmpData := make([]byte, bs)
		_, err = r.Read(tmpData)
		if err != nil {
			break
		}
		if binary.LittleEndian.Uint16(header[3]) == 1 {
			buf := bytes.NewBuffer(tmpData)
			gr, err := gzip.NewReader(buf)
			if err != nil {
				break
			}
			defer gr.Close()

			payloadBlock.Data, err = ioutil.ReadAll(gr)
			if err != nil {
				break
			}

		} else {
			payloadBlock.Data = make([]byte, len(tmpData))
			payloadBlock.Data = tmpData
		}

		payloadBlocks = append(payloadBlocks, &payloadBlock)
	}

	if err == io.EOF {
		err = nil
	}

	return payloadBlocks, err
}

// payload returns payload area (from end of header until file end)
// Seeker must be at correct position, before calling this
func payload(f io.ReadSeeker) ([]byte, error) {
	return ioutil.ReadAll(f)
}

func entries(header map[byte][]byte, xmlData []byte) (EntryList, error) {
	var r EntryList
	err := xml.Unmarshal(xmlData, &r)
	if err != nil {
		return r, err
	}

	r.ProtectedCount = 0
	var e *entry

	// TODO: is there a better way? perhaps need to use xpath library to avoid this
	r.GroupedEntries = make([]entry, len(r.RootEntries)+len(r.Entries))
	for _, entry := range r.RootEntries {
		r.GroupedEntries = append(r.GroupedEntries[:], entry)
	}
	for _, entry := range r.Entries {
		r.GroupedEntries = append(r.GroupedEntries[:], entry)
	}

	for i := range r.GroupedEntries {
		e = &r.GroupedEntries[i]
		count, err := e.handleData(&r.Buffer)
		if err != nil {
			return r, err
		}
		r.ProtectedCount += count
		var he *entry
		for hi := range e.HistoryEntries {
			he = &r.GroupedEntries[i].HistoryEntries[hi]
			count, err := he.handleData(&r.Buffer)
			if err != nil {
				return r, err
			}
			r.ProtectedCount += count
		}
	}

	return r, err
}
