package lib

// see https://gist.github.com/msmuenchen/9318327 for decryption steps

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"golang.org/x/crypto/salsa20"
	"unicode/utf8"
)

var nonce = []byte{
	0xe8,
	0x30,
	0x09,
	0x4b,
	0x97,
	0x20,
	0x5d,
	0x2a,
}

func compositeKey(password string) [32]byte {
	passwordHash := sha256.Sum256([]byte(password))
	return sha256.Sum256(passwordHash[:])
}

func masterKey(header map[byte][]byte, compositeKey [32]byte) ([32]byte, error) {
	transformSeed := header[5]
	transformRounds := binary.LittleEndian.Uint32(header[6])

	transformedKey := make([]byte, len(compositeKey))
	copy(transformedKey, compositeKey[:])

	var err error

	cipher, err := aes.NewCipher(transformSeed)
	size := cipher.BlockSize()
	if err != nil {
		return [32]byte{}, err
	}
	for i := 0; i < int(transformRounds); i++ {
		ciphertext := make([]byte, len(transformedKey))

		for bs, be := 0, size; bs < len(transformedKey); bs, be = bs+size, be+size {
			cipher.Encrypt(ciphertext[bs:be], transformedKey[bs:be])
		}
		transformedKey = ciphertext
	}

	var masterKey []byte
	masterSeed := header[4]
	for _, b := range masterSeed {
		masterKey = append(masterKey[:], b)
	}
	for _, b := range sha256.Sum256(transformedKey[:]) {
		masterKey = append(masterKey[:], b)
	}

	return sha256.Sum256(masterKey[:]), err
}

func decryptPayload(header map[byte][]byte, key [32]byte, payloadData []byte) ([]byte, error) {
	block, err := aes.NewCipher(key[:])
	if err != nil {
		return nil, err
	}

	mode := cipher.NewCBCDecrypter(block, header[7])
	res := make([]byte, len(payloadData))
	mode.CryptBlocks(res, payloadData)

	return res, err
}

func (e *entry) handleData(buffer *bytes.Buffer) (int, error) {
	protectedCount := 0
	for _, d := range e.Data {
		if d.Key == "Title" {
			e.Title = string(d.Value)
		}
		if d.Key == "Password" {
			protectedCount++
			decoded, err := base64.StdEncoding.DecodeString(string(d.Value))
			if err != nil {
				return 0, err
			}

			e.Size = len(decoded)
			_, err = buffer.Write(decoded)
			if err != nil {
				return 0, err
			}
		}
	}

	return protectedCount, nil
}

// marshalDecryptedPasswords sequentially marshals utf-8 strings from given byte array
// into current entry and it's children (history entries), based on entry.Size.
// Returns marshalled byte count.
func (e *entry) marshalDecryptedPasswords(res []byte) int {
	byteCount := 0

	pw := ""
	for bc := e.Size; bc > 0; bc-- {
		rune, size := utf8.DecodeRune(res[byteCount:])
		pw += string(rune)
		byteCount += size
	}
	e.PlainPassword = pw

	if len(e.HistoryEntries) > 0 {
		for i := 0; i < len(e.HistoryEntries); i++ {
			byteCount += e.HistoryEntries[i].marshalDecryptedPasswords(res[byteCount:])
		}
	}

	return byteCount
}

// DecryptAllEntries returns EntryList with decrypted password fields
// Takes EntryList to decrypt as well as PROTECTEDSTREAMKEY
func DecryptAllEntries(el *EntryList, protectedKey []byte) {
	key := sha256.Sum256(protectedKey)
	res := make([]byte, el.Buffer.Len())

	salsa20.XORKeyStream(res, el.Buffer.Bytes(), nonce, &key)

	startPos := 0
	for i := 0; i < len(el.GroupedEntries); i++ {
		startPos += el.GroupedEntries[i].marshalDecryptedPasswords(res[startPos:])
	}
}

// RawPasswordByTitle returns raw password, given EntryList and title of Entry
// Matches first entry by given title.
func RawPasswordByTitle(el *EntryList, protectedKey []byte, title string) string {
	key := sha256.Sum256(protectedKey)

	startPos := 0
	var i int
	for i = 0; i < len(el.GroupedEntries); i++ {
		e := &el.GroupedEntries[i]
		if e.Title == title {
			break
		}

		var f int
		for f = 0; f < len(e.HistoryEntries); f++ {
			he := e.HistoryEntries[f]
			if he.Title == title {
				break
			}

			startPos += he.Size
		}

		startPos += e.Size

	}

	res := make([]byte, el.Buffer.Len())

	salsa20.XORKeyStream(res, el.Buffer.Bytes(), nonce, &key)
	el.GroupedEntries[i].marshalDecryptedPasswords(res[startPos:])

	return el.GroupedEntries[i].PlainPassword
}
