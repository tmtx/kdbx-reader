package lib

import (
	"golang.org/x/crypto/ssh/terminal"
	"syscall"
)

// ReadPasswordFromStdin securly reads password from stdin
// wrapped here to avoid dependency in main package
func ReadPasswordFromStdin() ([]byte, error) {
	return terminal.ReadPassword(syscall.Stdin)
}
