package lib

import (
	"io"
)

// EncryptedEntryList returns list of kdbx entries with encrypted passwords, given path to kdbx file and raw password.
// Also returns PROTECTEDSTREAMKEY used for decrypting passwords.
func EncryptedEntryList(f io.ReadSeeker, password string) (EntryList, []byte, error) {
	var result EntryList

	header, err := Header(f, false)

	payload, err := payload(f)
	if err != nil {
		return result, nil, err
	}
	compositeKey := compositeKey(password)
	masterKey, err := masterKey(header, compositeKey)
	if err != nil {
		return result, nil, err
	}

	decryptedPayload, err := decryptPayload(header, masterKey, payload)
	if err != nil {
		return result, nil, err
	}

	payloadBlocks, err := payloadBlocks(header, decryptedPayload)
	if err != nil {
		return result, nil, err
	}

	result, err = entries(header, payloadBlocks[0].Data)
	return result, header[8], err
}
