package lib

import (
	"os"
	"path/filepath"
	"testing"
)

const (
	dbPath = "../test_data/test_db.kdbx"
	dbPw   = "test"
)

type testEntry struct {
	title    string
	password string
}

func TestRawPasswordByTitle(t *testing.T) {
	testCases := []testEntry{
		{"test entry 1", "XYHkYKU63vp"},
		{"test_entry_2", "cG+{kLxDaS3"},
	}

	path, err := filepath.Abs(dbPath)
	if err != nil {
		t.Errorf("Couldn't get absolute path from %s: %s", path, err)
	}

	f, err := os.Open(path)
	if err != nil {
		t.Errorf("Couldn't create file handle: %s", err)
	}
	defer f.Close()

	entryList, protectedKey, err := EncryptedEntryList(f, dbPw)
	if err != nil {
		t.Errorf("Couldn't get entry list: %s", err)
	}

	for _, testCase := range testCases {
		resPw := RawPasswordByTitle(&entryList, protectedKey, testCase.title)
		if resPw != testCase.password {
			t.Log("Passwords doesn't match")
			t.Logf("Entry title: %s", testCase.title)
			t.Logf("expected: %s", testCase.password)
			t.Logf("actual: %s", resPw)
			t.Fail()
		}
	}
}
